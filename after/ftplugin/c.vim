setlocal cinoptions=:0,l1,g0,t0,(0,W4,(s,m1,j1,J1,N-s
setlocal colorcolumn=80
setlocal shiftwidth=4
setlocal softtabstop=4
setlocal expandtab
setlocal number

let b:haslsp = v:false
if executable('cquery')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'cquery',
        \ 'cmd': {server_info->['cquery']},
        \ 'root_uri': {server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'compile_commands.json'))},
        \ 'initialization_options': { 'cacheDirectory': '/tmp/cquery/cache' },
        \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp', 'cc'],
        \ })
    let b:haslsp = v:true
elseif executable('clangd')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'clangd',
        \ 'cmd': {server_info->['clangd']},
        \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp'],
        \ })
    let b:haslsp = v:true
endif

if b:haslsp == v:true
    setlocal omnifunc=lsp#complete
endif
