setlocal colorcolumn=80
setlocal number

"" Enable when it's more mature.
"if executable('go-langserver')
"    au User lsp_setup call lsp#register_server({
"        \ 'name': 'go-langserver',
"        \ 'cmd': {server_info->['go-langserver', '-mode', 'stdio']},
"        \ 'whitelist': ['go'],
"        \ })
"
"    setlocal omnifunc=lsp#complete
"endif
