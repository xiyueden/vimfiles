setlocal colorcolumn=80
setlocal shiftwidth=4
setlocal softtabstop=4
setlocal expandtab
setlocal number

if executable('pyls')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'pyls',
        \ 'cmd': {server_info->['pyls']},
        \ 'whitelist': ['python'],
        \ })

    setlocal omnifunc=lsp#complete
endif

" ALE performs really bad if executables are on NFS. So manually set them
" here.
let b:ale_linters = []

if executable('flake8')
    let b:ale_linters += ['flake8']
    let b:ale_python_flake8_executable = system('echo -n `which flake8`')
    let b:ale_python_flake8_use_global = 1
endif

if executable('mypy')
    let b:ale_linters += ['mypy']
    let b:ale_python_mypy_executable = system('echo -n `which mypy`')
    let b:ale_python_mypy_use_global = 1
endif
