setlocal colorcolumn=80
setlocal shiftwidth=4
setlocal softtabstop=4
setlocal expandtab
setlocal number

" Change vim indent to be just one tab.
let g:vim_indent_cont = &sw
