colorscheme default
set background=dark
if &background == "dark"
	highlight Normal guibg=black guifg=white
	if has("gui_macvim")
		set transparency=10
	endif
endif
