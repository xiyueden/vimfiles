" Common global settings.
set nocompatible
set clipboard=unnamed

set encoding=utf-8
set fileencodings=utf-8,gb18030,big5,euc-tw,iso88591,koi8-t
set autoread

syntax on
filetype plugin indent on

" Highlight settings.
autocmd ColorScheme * highlight clear SpellBad
autocmd ColorScheme * highlight SpellBad term=underline cterm=underline

" Color theme
colorscheme zenburn
set background=dark

" Highlight tabs, trailing spaces.
set list!
set listchars=tab:▶\ ,trail:.,extends:\#,nbsp:.

" Spell settings
set spell

" Search related options.
set hlsearch
set incsearch
set ignorecase
set smartcase
set wrapscan

" Bottom displays.
set showcmd
set showmode
set ruler
set laststatus=2

" Global formatting.
set formatoptions+=Mm " For Chinese characters autowrap.
set noexpandtab
set autoindent

" Autoformat
nnoremap <silent> <leader>fmt   :Autoformat<cr>

" ale
let g:ale_python_mypy_options = '--ignore-missing-stubs'
let g:ale_lint_on_text_changed = 0
nnoremap <silent> <leader>aj    :ALENext<cr>
nnoremap <silent> <leader>ak    :ALEPrevious<cr>
nnoremap <silent> <leader>af    :ALEFixSuggest<cr>

" vim-lsp
nnoremap <silent> <leader>gD    :LspDocumentDiagnostics<cr>
nnoremap <silent> <leader>gd    :LspDefinition<cr>
nnoremap <silent> <leader>gf    :LspDocumentFormat<cr>
vnoremap <silent> <leader>gf    :LspDocumentRangeFormat<cr>
nnoremap <silent> <leader>go    :LspDocumentSymbol<cr>
nnoremap <silent> <leader>gh    :LspHover<cr>
nnoremap <silent> <leader>gi    :LspImplementation<cr>
nnoremap <silent> <leader>gr    :LspReferences<cr>
nnoremap <silent> <leader>gn    :LspRename<cr>
nnoremap <silent> <leader>gs    :LspWorkspaceSymbol<cr>
